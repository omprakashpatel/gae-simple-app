
from google.appengine.ext import ndb

class BaseClass(object):
	"""docstring for BaseClass"""
	@classmethod
	def get_one(cls, id):
		key = ndb.Key(cls, id)
		return key.get()

	@classmethod
	def get_all(cls, filter_args={}):
		filter_args = filter_args if filter_args is not None else {}
		return cls.query(**filter_args)

class Account(BaseClass, ndb.Model):
	""" User account class """
	id = ndb.StringProperty()
	name = ndb.StringProperty(required=True)
	email = ndb.StringProperty(required=True)
	is_verified = ndb.BooleanProperty(default=False)
	picture = ndb.StringProperty()

class PostModel(BaseClass, ndb.Model):
	""" User account class """
	title = ndb.StringProperty(required=True)
	post = ndb.StringProperty()
	file_type = ndb.StringProperty(required=True)
	file_key = ndb.BlobKeyProperty(required=True)
	account_id = ndb.KeyProperty(Account)
	created_on = ndb.DateTimeProperty(auto_now_add=True)
	updated_on = ndb.DateTimeProperty(auto_now_add=True)
	comment_count = ndb.IntegerProperty(default = 0)
	like_count = ndb.IntegerProperty(default = 0)
	reject_count = ndb.IntegerProperty(default = 0)


class Comment(BaseClass, ndb.Model):
	""" """
	comment = ndb.StringProperty(required=True)
	post_id = ndb.KeyProperty(PostModel, required=True)
	account_id = ndb.KeyProperty(Account)
	created_on = ndb.DateTimeProperty(auto_now_add=True)
	updated_on = ndb.DateTimeProperty(auto_now_add=True)


class LikeReject(BaseClass, ndb.Model):
	"""docstring for LikePost"""
	id = ndb.StringProperty() # postid_userid
	post_id = ndb.KeyProperty(PostModel)
	user_id = ndb.KeyProperty(Account)
	is_like = ndb.BooleanProperty(default = False)
	is_reject = ndb.BooleanProperty(default = False)
	created_on = ndb.DateTimeProperty(auto_now_add=True)
	updated_on = ndb.DateTimeProperty(auto_now_add=True)
