import webapp2
from google.appengine.api import mail
from config import DEV, ADMIN, MAIL_CONFIG


def send_mail(email, subject, body):
    try:
        if not isinstance(email, list):
            email = [email]
        for _mail in email:
            if mail.is_email_valid(_mail):
                mail.send_mail(MAIL_CONFIG['MAIL_DEFAULT_SENDER'],_mail,subject,body)
    except Exception, e:
        raise e
    return True

def send_mail2(email, subject, html):
    try:
        if not isinstance(email, list):
            email = [email]
        message = mail.EmailMessage(sender=MAIL_CONFIG['MAIL_DEFAULT_SENDER'],subject=subject)
        message.to = email
        message.html = html
        message.send()
    except Exception, e:
        raise e
    return True

def log_info_send_email_task(data):
    print("****** Started Email Task *********************")
    try:
        html = """
            <div style="max-widht:95%;background-color:#f7f7f7">
                <table style="max-widht:95%; border: 1px solid #222;">
                    <tr><td>Post Count</td><td>{}</td></tr>
                    <tr><td>Comment Count</td><td>{}</td></tr>
                    <tr><td>Like Count</td><td>{}</td></tr>
                    <tr><td>Reject Count</td><td>{}</td></tr>
                </table>
            </div>
        """.format(data.get('post_count', '0'), data.get('comment_count','0'),
                data.get('like_count', '0'), data.get('reject_count', '0'))
        subject = "Log Info"
        send_mail2(ADMIN, subject, html)
    except Exception, e:
        print("****** Exception Email Task *********************")
        print str(e)
        raise e
    print("****** Success Email Task *********************")
    return True

# ==============================================================================
# Testing email functions
# ==============================================================================
def new_post_send_email_task(data):
    # EMAIL task
    print("Sending Email")
    for _mail in DEV:
        if mail.is_email_valid(_mail):
            body = """
                =====================
                Post Count: {}
                Comment Count: {}
                Like Count: {}
                Reject Count: {}
                =====================
            """.format(data.get('post_count', 0), data.get('comment_count', 0), data.get('like_count', 0), data.get('reject_count', 0))
            mail.send_mail(MAIL_CONFIG['MAIL_DEFAULT_SENDER'],_mail,"Log Info", body)
    return True

# ==============================================================================

def new_post_send_email_task_2():
    message = mail.EmailMessage(sender="Example.com Support <mail@om-prakash.appspotmail.com>",
                            subject="New Post Receinved")

    message.to = DEV
    message.body = """
    Dear om-prakash:
        New Post is received
        =====================
        The znapin Team
    """
    message.send()


# ==============================================================================
