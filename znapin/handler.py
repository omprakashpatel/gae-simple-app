import os
import logging

from google.appengine.api import users
from google.appengine.ext import ndb
from google.appengine.ext import blobstore
from google.appengine.ext.webapp import blobstore_handlers


from datetime import datetime, timedelta
import jinja2
import webapp2
import pdb
import json
from znapin.models import Account, PostModel, Comment, LikeReject
from znapin.email import new_post_send_email_task, log_info_send_email_task

JINJA_ENVIRONMENT = jinja2.Environment(loader=jinja2.FileSystemLoader('templates'))

# ==============================================================================
# Jinja filters
def datetimeformat(value, format="%H:%M %d %b %Y"):
	return value.strftime(format)

JINJA_ENVIRONMENT.filters['datetimeformat'] = datetimeformat

# ==============================================================================
def login_required():
	user = users.get_current_user()
	template_values = {}
	if user:
		_account = Account.get_one(user.user_id())
		template_values = {
			'user': user,
			'account': _account,
			'url': users.create_logout_url('/'),
			'url_linktext': 'Logout',
		}
	else:
		template_values = {
				'url': "/login",
				'url_linktext': 'Login'
			}
	return template_values


class MainPage(webapp2.RequestHandler):
	def get(self):
		user = users.get_current_user()
		template_values = login_required()
		_posts = PostModel.get_all()
		_post_list = _posts.order(-PostModel.created_on).fetch(100)
		for _post in _post_list:
			_post.user = _post.account_id.get()
		template_values['post_list'] = _post_list or []
		template = JINJA_ENVIRONMENT.get_template('index.html')
		self.response.write(template.render(template_values))


class Profile(webapp2.RequestHandler):
	"""docstring for Profile"""
	def get(self):
		template_values = login_required()
		if not template_values.get('user'):
			self.redirect('/')
		template_values['user_list'] = Account.get_all().fetch(100)
		template = JINJA_ENVIRONMENT.get_template('profile.html')
		self.response.write(template.render(template_values))

class PostPage(webapp2.RequestHandler):
	""" docstring for Post """
	def get(self, message=None):
		template_values = login_required()
		if not template_values.get('user'):
			self.redirect('/login')

		upload_url = blobstore.create_upload_url('/post_submit')
		template_values['upload_url'] = upload_url
		template = JINJA_ENVIRONMENT.get_template('post.html')
		self.response.write(template.render(template_values))


class PostSubmit(blobstore_handlers.BlobstoreUploadHandler):
	"""docstring for PostSubmit"""
	def post(self):
		template_values = login_required()
		if not template_values.get('user'):
			self.redirect('/login')
		_data = {}
		try:
			upload = self.get_uploads()
			if not upload:
				self.error(500)
			_data = {
				'title': self.request.get('title'),
				'post': self.request.get('post'),
				'file_type': self.request.get('file_type'),
				'file_key': upload[0].key()
			}
			if upload[0].content_type.split('/')[0] == "image":
				_data['file_type'] = "image"
			elif upload[0].content_type.split('/')[0] == "video":
				_data['file_type'] = "video"

			_post = PostModel(title=_data['title'],
						post=_data['post'],
						file_type=_data['file_type'],
						file_key=_data['file_key'],
						account_id=template_values['account'].key)

			_post.put()
		except Exception, e:
			print e
			self.error(500)
		self.redirect('/post')


class ViewPhotoHandler(blobstore_handlers.BlobstoreDownloadHandler):
    def get(self, photo_key):
        if not blobstore.get(photo_key):
            self.error(404)
        else:
            self.send_blob(photo_key)


class BrowsePage(webapp2.RequestHandler):
	@classmethod
	def get_comment_by_post_key(cls, post_key):
		# _comment = Comment.get_all({'post_id': post_key})
		_comment_obj = Comment.query(Comment.post_id == post_key)
		_comment_list = _comment_obj.order(-Comment.created_on).fetch(10) or []
		for _comment in _comment_list:
			_comment.user = _comment.account_id.get()
		return _comment_list

	def get(self):
		template_values = login_required()
		post_key = self.request.get('key')
		if not post_key:
			self.redirect('/')

		_post_key = ndb.Key(urlsafe=post_key)
		# getting post data
		_post_data = _post_key.get()
		# gettting user data
		_post_data.user = _post_data.account_id.get()

		template_values['post'] = _post_data
		template_values['comment_list'] = self.get_comment_by_post_key(_post_key)

		print template_values

		template = JINJA_ENVIRONMENT.get_template('browse_post.html')
		self.response.write(template.render(template_values))


class CommentHandler(webapp2.RequestHandler):
	"""docstring for CommentHandler"""
	def get(self):
		self.response.write("Comment GET Function")

	def post(self):
		template_values = login_required()
		if not template_values.get('user'):
			self.redirect('/login')

		post_id = self.request.get('post_id')

		if post_id:
			post_key = ndb.Key(urlsafe=post_id)
			_comment_data = {
				'comment': self.request.get('comment'),
				'post_id': post_key,
				'account_id': template_values['account'].key
			}

			_comment = Comment(comment=_comment_data['comment'],
					post_id=_comment_data['post_id'],
					account_id=_comment_data['account_id'])

			_comment_key = _comment.put()

		self.redirect("/browse?key={}".format(post_id))

class CommentAjaxHandler(webapp2.RequestHandler):
	"""docstring for CommentHandler"""
	def get(self):
		self.response.write("Comment GET Function")

	def post(self):
		template_values = login_required()
		if not template_values.get('user'):
			self.redirect('/login')

		data = {
			'post_id': self.request.get('post_id'),
			'comment': self.request.get('comment')
		}
		if not data.get('post_id'):
			self.response.write("Invalid post_id")
		if not data.get('comment'):
			print "Comment not fount"
			self.response.write("Not comment found")
		# post_key = ndb.Key(PostModel, data['post_id'])
		post_key = ndb.Key(urlsafe=data['post_id'])

		_comment_data = {
			'comment': data.get('comment') or None,
			'post_id': post_key,
			'account_id': template_values['account'].key
		}

		try:
			_comment = Comment(comment=_comment_data['comment'],
					post_id=_comment_data['post_id'],
					account_id=_comment_data['account_id'])

			_comment_key = _comment.put()

			_post = post_key.get()
			_post.comment_count += 1
			_post.put()

		except Exception, e:
			raise e

		_template = """ <div class="col-md-12">
							<div class="well">
							<p>{comment}</p>
							<footer class="small">
							<span >
							Posted by: <cite title="Source Title">{user_name}</cite>
							</span>
							<span class="pull-right">
							Posted on: <cite>{created_on}</cite>
							</span>
							</footer>
							</div>
							</div>""".format(comment=_comment.comment,
										user_name=template_values['account'].name,
										created_on=datetimeformat(_comment.created_on))

		self.response.out.write(json.dumps({'status': 'success', 'data': _template}))

class LikeApi(webapp2.RequestHandler):
	def get(self):
		print "GET Method LikeRejectApi"

	def post(self):
		print "Post Method LikeRejectApi"
		template_values = login_required()
		if not template_values.get('user'):
			self.redirect('/login')
		post_id_string = self.request.get('post_id')
		_post_key = ndb.Key(urlsafe=post_id_string)

		user_key = template_values['account'].key
		user_id = user_key.id()

		like_reject_id = "{}_{}".format(_post_key.id(), user_id)
		like_reject_key = ndb.Key(LikeReject, like_reject_id)
		_like_reject_data = like_reject_key.get()

		_post_update = {'like_count': 0, 'reject_count': 0}
		if _like_reject_data:
			if not _like_reject_data.is_like == True:
				_post_update['like_count'] = 1
				if _like_reject_data.is_reject == True:
					_post_update['reject_count'] = -1

			_like_reject_data.is_like = True
			_like_reject_data.is_reject = False
			_like_reject_data.updated_on = datetime.utcnow()
			_like_reject_data.put()
		else:
			_like_reject_data = LikeReject(key=like_reject_key,
				post_id=_post_key,
				user_id=user_key,
				is_like=True,
				is_reject=False)
			_like_reject_data.put()
			_post_update['like_count'] = 1


		# updateting Post properties -> like_count, reject_count
		_post = _post_key.get()
		_post.like_count = (_post.like_count or 0) + _post_update['like_count']
		_post.reject_count = (_post.reject_count or 0) + _post_update['reject_count']
		_post.put()

		self.response.out.write(json.dumps({'status': 'success', 'data': _post_update}))


class RejectApi(webapp2.RequestHandler):
	def get(self):
		print "GET Method LikeRejectApi"

	def post(self):
		print "put Method LikeRejectApi"
		template_values = login_required()
		if not template_values.get('user'):
			self.redirect('/login')
		post_id_string = self.request.get('post_id')
		_post_key = ndb.Key(urlsafe=post_id_string)

		user_key = template_values['account'].key
		user_id = user_key.id()

		like_reject_id = "{}_{}".format(_post_key.id(), user_id)
		like_reject_key = ndb.Key(LikeReject, like_reject_id)
		_like_reject_data = like_reject_key.get()

		_post_update = {'like_count': 0, 'reject_count': 0}
		if _like_reject_data:
			if not _like_reject_data.is_reject == True:
				_post_update['reject_count'] = 1
				if _like_reject_data.is_like == True:
					_post_update['like_count'] = -1

			_like_reject_data.is_like = False
			_like_reject_data.is_reject = True
			_like_reject_data.updated_on = datetime.utcnow()
			_like_reject_data.put()
		else:
			_like_reject_data = LikeReject(key=like_reject_key,
				post_id=_post_key,
				user_id=user_key,
				is_like=False,
				is_reject=True)
			_like_reject_data.put()
			_post_update['reject_count'] = 1


		# updateting Post properties -> like_count, reject_count
		_post = _post_key.get()
		_post.like_count = (_post.like_count or 0) + _post_update['like_count']
		_post.reject_count = (_post.reject_count or 0) + _post_update['reject_count']
		_post.put()

		self.response.out.write(json.dumps({'status': 'success', 'data': _post_update}))


# ==============================================================================

class LoginPage(webapp2.RequestHandler):
	"""docstring for ClassName"""
	def get(self):
		user = users.get_current_user()
		if user:
			self.redirect('/')
		else:
			self.redirect(users.create_login_url('/signupupdate'))

	def post(self):
		self.response.write("Login POST Method")

class SignupUpdate(webapp2.RequestHandler):
	def get(self):
		user = users.get_current_user()
		if user:
			print user.user_id()
			_account = Account(
					id=user.user_id(),
					name=user.nickname(),
					email=user.email(),
					is_verified=True)

			_user_key = ndb.Key(Account, user.user_id())
			_account.key = _user_key
			_user = _user_key.get()
			if _user:
				print "Already Registered"
				print _user
			else:
				try:
					_user_key = _account.put()
				except Exception, e:
					print "******************************"
					print str(e)
					raise e
			self.redirect('/')

		else:
			self.redirect('/login')

class SignupPage(webapp2.RequestHandler):
	""" """
	def get(self):
		self.response.write("SignupPage GET Method")
	def post(self):
		self.response.write("SignupPage POST Method")


class Logout(webapp2.RequestHandler):
	"""docstring for Logout """
	def get(self):
		user = users.get_current_user()
		if user:
			print "*** Logged out ***"
			self.redirect(users.create_logout_url('/'))
		else:
			print "*** Already Logged out ***"
			self.redirect('/')

# cron tasks
class MailTask(webapp2.RequestHandler):
	""" """
	def get(self):
		end_datetime = datetime.utcnow().replace(minute=0, second=0, microsecond=0)
		start_datetime = end_datetime - timedelta(hours=1)

		_data = {}
		_posts = PostModel.query(PostModel.created_on > start_datetime,  PostModel.created_on < end_datetime)
		_data['post_count'] = _posts.count()

		_comment_obj = Comment.query(Comment.created_on > start_datetime,  Comment.created_on < end_datetime)
		_data['comment_count'] = _comment_obj.count()

		_like1 = LikeReject.query()
		_like2 = _like1.filter(LikeReject.created_on >= start_datetime)
		_like3 = _like2.filter(LikeReject.created_on < end_datetime)
		_like = _like3.filter(LikeReject.is_like==True)
		_data['like_count'] = _like.count()

		_reject1 = LikeReject.query()
		_reject2 = _reject1.filter(LikeReject.created_on >= start_datetime)
		_reject3 = _reject2.filter(LikeReject.created_on < end_datetime)
		_reject = _reject3.filter(LikeReject.is_reject==True)
		_data['reject_count'] = _reject.count()

		log_info_send_email_task(data=_data)
		new_post_send_email_task(data=_data)
		self.response.out.write(json.dumps({'status': 'success', 'data': _data}))
