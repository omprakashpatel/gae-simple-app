import webapp2
from znapin.handler import *
from config import app_config

# ==============================================================================
route_list = [
		('/', MainPage),
		('/profile', Profile),
		('/login', LoginPage),
		('/signup', SignupPage),
		('/logout', Logout),
		('/signupupdate', SignupUpdate),
		('/post', PostPage),
		('/post_submit', PostSubmit),
		('/upload/([^/]+)?', ViewPhotoHandler),
		('/browse', BrowsePage),
		('/comment', CommentHandler),
		('/comment_ajax', CommentAjaxHandler),
		('/like', LikeApi),
		('/reject', RejectApi),
		('/mail/hourly', MailTask)
]

app = webapp2.WSGIApplication(route_list, debug=True)
