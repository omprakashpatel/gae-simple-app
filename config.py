app_config = {
	'API_KEY': 'ZNAPIN API KEY',
	'debug': True
}

UPLOAD_FOLDER = "/home/om/dev/googleapp-znapin/static/upload"
ALLOWED_FILE_EXTENSIONS = ['']

DEV = ['omprakashpatelcs@gmail.com']
ADMIN = ['omprakashpatelcs@gmail.com']

MAIL_CONFIG = {
    'MAIL_DEFAULT_SENDER': 'Znapin <info@om-prakash.appspotmail.com>'
}